<?php

require_once "exceptions/FileException.php";

// Clase utilizada para subir fotos al servidor y a la base de datos
class File {

    private $file;
    private $fileName;

    public function __construct(string $fileName, array $arrTypes)
    {
        $this->file = $_FILES[$fileName];

        $this->fileName = $this->file['name'];

        if(($this->file["name"] == "")) {

            throw new FileException("Debes especificar un fichero");

        }

        if($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException("El fichero es demasiado pesado");
                break;

                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("El fichero no pudo subirse por completo");
                break;

                default:
                    throw new FileException("Hubo un error en la subida del fichero");
                break;

            }

        }

        if(in_array($this->file["type"], $arrTypes) === false) {

            throw new FileException("El fichero no tiene un formato adecuado");

        }

    }

    public function getFileName() {

        return $this->fileName;

    }

    // Guarda la foto subida a la ruta indicada si es posible
    public function saveUploadFile(string $ruta) {

        if(!is_uploaded_file($this->file["tmp_name"])) {

            throw new FileException("El archivo no se ha subido mediante formulario");

        }

        $rutaDestino = $ruta.$this->fileName;

        if(is_file($rutaDestino)) {

            $idUnico = time();
            $this->fileName = $idUnico . $this->fileName;
            $rutaDestino = $ruta . $this->fileName;

        }

        if(move_uploaded_file($this->file["tmp_name"], $rutaDestino) === false) {

            throw new FileException("No se ha podido mover el fichero al destino especificado");

        }

    }

    // Copia una foto de una ruta indicada a otra, si es posible
    public function copyFile($rutaOrigen, $rutaFinal) {

        $ficheroOrigen = $rutaOrigen.$this->fileName;
        $ficheroDestino = $rutaFinal.$this->fileName;

        if(!is_file($ficheroOrigen)) {

            throw new FileException("No existe el fichero $ficheroOrigen");

        }

        if(is_file($rutaFinal)) {

            throw new FileException("El fichero $ficheroDestino ya existe");

        }

        if(copy($ficheroOrigen, $ficheroDestino) === false) {

            throw new FileException("No se ha podido copiar el fichero");

        }

    }

}

?>