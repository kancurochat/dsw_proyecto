<?php

require_once "database/IEntity.php";

// Clase que representa a cada registro de la tabla "imagenes" de la base de datos
class ImagenGaleria implements IEntity {
    
    private $id;

    private $nombre;

    private $descripcion;

    private $categoria;

    private const RUTA_IMAGENES_PORT1 = 'img/portfolio/';

    private const RUTA_IMAGENES_PORT2 = 'img/portfolio-2/';

    public function __construct($id=0, $nombre='', $descripcion='', $categoria='')
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->categoria = $categoria;
    }



    public function toArray(): array
    {
        return [

            "id" => $this->getId(),

            "nombre" => $this->getNombre(),

            "categoria" => $this->getCategoria(),

            "descripcion" => $this->getDescripcion()

        ];
    }

    public function getURLPort1() {

        return self::RUTA_IMAGENES_PORT1 . $this->getNombre();

    }

    public function getURLPort2() {

        return self::RUTA_IMAGENES_PORT2 . $this->getNombre();

    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }

    // Función que convierte el id de la categoria de cada foto en su respectivo nombre
    public function getNombreCategoria()
    {
        switch ($this->categoria) {

            case 1:
                $result = 'nature';
            break;
            
            case 2:
                $result = 'studio';
            break;

            case 3:
                $result = 'weddings';
            break;

            case 4:
                $result = 'lifestyle';
            break;

            case 5:
                $result = 'fashion';
            break;

        }
        return $result;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */ 
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }
}

?>