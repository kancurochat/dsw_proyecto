<?php

// Clase que representa cada registro de la tabla "categorias" en la base de datos
class Categoria implements IEntity {
    
    private $id;

    private $nombre;

    public function __construct($id=0, $nombre="")
    {
        $this->id = $id;
        $this->nombre = $nombre;
    }



    public function toArray(): array
    {
        
        return [

            "id" => $this->getId(),

            "nombre" => $this->getNombre()

        ];

    }


    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
}

?>