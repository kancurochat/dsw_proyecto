<?php 

class Request {

    // Nos devuelve la URI sin barras al principio ni al final
    public static function uri() {

        return trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");

    }

}

?>