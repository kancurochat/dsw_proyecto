<?php

// Iniciamos la sesión
session_start();

// Importamos lo que vamos a usar desde el principio
require_once "App.php";
require_once "Request.php";
require_once "vendor/autoload.php";
require_once "utils/MyLog.php";
require_once "repository/ImagenGaleriaRepository.php";

// Importamos la configuración de la base de datos
$config = require_once "app/config.php";

// Creamos una instancia de la clase MyLog
$logger = MyLog::load('logs/info.log');

// Añadimos todo al contenedor de servicios
App::bind("config", $config);
App::bind("logger", $logger);

// Creo una instancia del repositorio de imágenes
$imagenGaleriaRepository = new ImagenGaleriaRepository();

?>