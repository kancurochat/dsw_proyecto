<?php

require_once __DIR__ . "../../exceptions/AppException.php";
require_once __DIR__ . "../../database/Connection.php";

class App {

    //Almacena los datos en nuestro contenedor

    private static $container = [];

    // Crea un nuevo par clave-valor en nuestro contenedor de servicios
    public static function bind (string $key, $value) {

        static::$container[$key] = $value;

    }

    // Obtenemos el valor asociado a una clave
    public static function get(string $key) {

        if(!array_key_exists($key, static::$container)) {

            throw new AppException("No se ha encontrado la clave $key en el contenedor.");

        }

        return static::$container[$key];

    }

    // Obtenemos la conexión con la base de datos
    public static function getConnection() {

        if(!array_key_exists("connection", static::$container)) {

            static::$container["connection"] = Connection::make();

        }

        return static::$container["connection"];

    }

}

?>