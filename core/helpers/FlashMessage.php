<?php

class FlashMessage {

    // Obtenemos el valor asociado a una clave dentro del array de la sesión del usuario
    public static function get(string $key, $default="") {

        if(isset($_SESSION["flash-message"])) {

            $value = $_SESSION["flash-message"][$key] ?? $default;

            unset($_SESSION["flash-message"][$key]);

        }else {

            $value = $default;

        }

        return $value;

    }

    // Añadimos un par clave-valor nuevo en la sesión
    public static function set(string $key, $value) {

        $_SESSION["flash-message"][$key] = $value;

    }

    // Deshacemos un par clave-valor 
    public static function unset(string $key) {

        if(isset($_SESSION["flash-message"])) {

            unset($_SESSION["flash-message"][$key]);

        }

    }

}

?>