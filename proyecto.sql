-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `categorias` (`id`, `nombre`) VALUES
(1,	'nature'),
(2,	'studio'),
(3,	'weddings'),
(4,	'lifestyle'),
(5,	'fashion');

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE `imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `categoria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria` (`categoria`),
  CONSTRAINT `imagenes_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `imagenes` (`id`, `nombre`, `descripcion`, `categoria`) VALUES
(1,	'1.jpg',	'',	1),
(8,	'2.jpg',	'',	2),
(9,	'3.jpg',	'',	3),
(10,	'4.jpg',	'',	4),
(11,	'5.jpg',	'',	5),
(12,	'6.jpg',	'',	1),
(13,	'7.jpg',	'',	2),
(14,	'8.jpg',	'',	3),
(15,	'9.jpg',	'',	4),
(16,	'10.jpg',	'',	5),
(17,	'11.jpg',	'',	3),
(18,	'12.jpg',	'',	1),
(19,	'13.jpg',	'',	5),
(20,	'14.jpg',	'',	2),
(21,	'15.jpg',	'',	5),
(22,	'16.jpg',	'',	1),
(23,	'17.jpg',	'',	3),
(24,	'18.jpg',	'',	2),
(25,	'19.jpg',	'',	4),
(26,	'20.jpg',	'',	5),
(27,	'21.jpg',	'',	1),
(28,	'22.jpg',	'',	2),
(29,	'23.jpg',	'',	5);

DROP TABLE IF EXISTS `mensajes`;
CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `asunto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


-- 2020-12-10 12:49:47
