<?php

// Interfaz que nos permite insertar las entidades en la base de datos

interface IEntity {

    public function toArray(): array;

}

?>