<?php

require_once __DIR__ . "../../exceptions/AppException.php";
require_once __DIR__ . "../../core/App.php";

class Connection {

    // Establece y devuelve una conexión con la base de datos usando nuestro contenedor
    // de servicios. Si no lo consigue, lanza una excepción
    public static function make() {

        try {

            $config = App::get("config")["database"];

            $connection = new PDO(

                $config['connection'] . ";dbname=" . $config['name'],

                $config['username'],

                $config['password'],

                $config['options']

            );

        }catch (PDOException $PDOException) {

            throw new AppException("No se ha podido conectar con la BBDD.");

        }

        return $connection;

    }

}

?>