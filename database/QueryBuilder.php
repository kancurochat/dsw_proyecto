<?php

require_once "exceptions/QueryException.php";
require_once "exceptions/NotFoundException.php";
require_once "core/App.php";

// Clase abstracta que otorga la funcionalidad a las clases repositorio
// para insertar y consultar valores de la base de datos

abstract class QueryBuilder {

    private $connection;

    private $table;

    private $classEntity;

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    // Prepara una consulta preparada y la ejecuta si es posible
    public function executeQuery(string $sql) {

        $pdoStatement = $this->connection->prepare($sql);

        if($pdoStatement->execute() === false) {

            throw new QueryException("No se ha podido ejecutar la consulta");

        }

        // Devuelve un array de objetos
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);

    }

    // Devuelve un array con todos los objetos de la base de datos
    public function findAll() {

        $sql = "SELECT * FROM $this->table";

        $result = $this->executeQuery($sql);

        return $result;

    }

    public function findAllLimit(int $limit) {

        $sql = "SELECT * FROM $this->table LIMIT $limit";

        $result = $this->executeQuery($sql);

        return $result;

    }

    // Almacena un objeto en la base de datos
    public function save(IEntity $entity): void {

        try {
            
            $parameters = $entity->toArray();

            $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)",
            
                $this->table,

                implode(", ", array_keys($parameters)), 

                ":". implode(", :", array_keys($parameters))
            
            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);

        } catch (PDOException $exception) {
            
            throw new QueryException("Error al insertar en la BBDD.");

        }

    }

    // Busca un objeto mediante id en la base de datos
    public function find(int $id) : IEntity {

        $sql = "SELECT * FROM $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if(empty($result)) {

            throw new NotFoundException("No se ha encotnrado el elemento con id $id");

        }

        return $result[0];

    }



}

?>