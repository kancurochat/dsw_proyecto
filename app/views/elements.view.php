<?php 

require_once "partials/inicio-doc.part.php";

require_once "utils/utils.php";

?>

	<!-- Header section  -->
	<?php require_once "partials/nav.part.php"; ?>
	<div class="clearfix"></div>
	<!-- Header section end  -->

	<!-- Contact section  -->
	<section class="elements-section">
		<div class="container">
		<?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if(empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>
			<h2>Post a photo!</h2>
			<form class="contact-form p-5" method="POST" action="" enctype="multipart/form-data">
							<div class="row">
								<div class="col-lg-6">
									<input name="imagen" type="file" class="form-control-file">
								</div>
								<div class="col-lg-6">
									<label for="categoria">Category</label>
									<select name="categoria" id="" class="form-control">

										<?php foreach($categorias as $categoria) : ?>

											<option value="<?= $categoria->getId() ?>" <?= $categoriaSeleccionada == $categoria->getId() ? 'selected' : '' ?> ><?= $categoria->getNombre() ?></option>

										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-lg-4">
								</div>
								<div class="col-lg-12">
									<textarea name="descripcion" class="text-msg" placeholder="Description"><?= $descripcion ?? '' ?></textarea>
									<button class="site-btn" type="submit">send photo</button>
								</div>
							</div>
						</form>
			</div>
	</section>
	<!-- Elements section end  -->
	
	<!-- Footer section   -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 order-1 order-md-2">
					<div class="footer-social-links">
						<a href=""><i class="fa fa-pinterest"></i></a>
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-dribbble"></i></a>
						<a href=""><i class="fa fa-behance"></i></a>
					</div>
				</div>
				<div class="col-md-6 order-2 order-md-1">
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>	
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end  -->

	<!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->

	<?php 

require_once "partials/fin-doc.part.php";

?>