<?php 

require_once "partials/inicio-doc.part.php";

require_once "utils/utils.php";

?>

	<!-- Header section  -->
	<?php require_once "partials/nav.part.php"; ?>
	<div class="clearfix"></div>
	<!-- Header section end  -->

	<!-- Google Map -->
	<div class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14025.369902427043!2d-13.870635161886922!3d28.499342119527054!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc47c63156cfa847%3A0xab001352b65dd9a1!2sPuerto%20del%20Rosario%2C%20Las%20Palmas!5e0!3m2!1ses!2ses!4v1607001070514!5m2!1ses!2ses" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	</div>
	<!-- Google Map end -->

	<!-- Contact section  -->
	<section class="contact-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="contact-text">
						<h3>Get in touch</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas.</p>
						<ul>
							<li>Main Str, no 23, NY, <br>New York PK 23589</li>
							<li>+546 990221 123</li>
							<li>contact@industryalinc.com</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-8">
					<form class="contact-form" method="POST">
						<div class="row">
							<div class="col-lg-6">
								<input type="text" placeholder="Your Name" name="name">
							</div>
							<div class="col-lg-6">
								<input type="text" placeholder="Your Email" name="email">
							</div>
							<div class="col-lg-4">
							</div>
							<div class="col-lg-12">
								<input type="text" placeholder="Subject" name="subject">
								<textarea class="text-msg" placeholder="Message" name="message"></textarea>
								<button class="site-btn" type="submit">send message</button>
							</div>
						</div>
					</form>
				</div>
				<?= $div; ?>
			</div>
			<table class="table mt-5">
				<tr>
					<th>Nombre</th>
					<th>Email</th>
					<th>Asunto</th>
					<th>Mensaje</th>
				</tr>
				<?php 
				
					require_once "app/views/partials/table-tr.part.php";
				
				?>
			</table>
		</div>
	</section>
	<!-- Contact section end  -->

	<!-- Instagram section -->
	<div class="instagram-section">
		<h6>Instagram Feed</h6>
		<div id="instafeed" class="instagram-slider owl-carousel"></div>
	</div>
	<!-- Instagram section end -->
	
	<!-- Footer section   -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 order-1 order-md-2">
					<div class="footer-social-links">
						<a href=""><i class="fa fa-pinterest"></i></a>
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-dribbble"></i></a>
						<a href=""><i class="fa fa-behance"></i></a>
					</div>
				</div>
				<div class="col-md-6 order-2 order-md-1">
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>	
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end  -->

	<!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->

	<?php 

require_once "partials/fin-doc.part.php";

?>