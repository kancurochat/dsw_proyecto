<?php 

require_once "partials/inicio-doc.part.php";

require_once "utils/utils.php";

?>

	<?php require_once "partials/nav.part.php"; ?>
	
	<div class="clearfix"></div>
	<!-- Header section end  -->

	<!-- Hero section  -->
	<div class="hero-section">
		<div class="hero-slider owl-carousel">
			
		<?php require "app/views/partials/hero-item.part.php"; ?>

		</div>
		<div class="hero-slider owl-carousel">

			<?php require "app/views/partials/hero-item.part.php"; ?>

		</div>
		<div class="hero-slider owl-carousel">

		<?php require "app/views/partials/hero-item.part.php"; ?>
		
		</div>
		<div class="hero-social-links">
			<a href=""><i class="fa fa-pinterest"></i></a>
			<a href=""><i class="fa fa-facebook"></i></a>
			<a href=""><i class="fa fa-twitter"></i></a>
			<a href=""><i class="fa fa-dribbble"></i></a>
			<a href=""><i class="fa fa-behance"></i></a>
		</div>
	</div>
	<!-- Hero section end  -->
	
	<!-- Intro section   -->
	<section class="intro-section">
		<div class="intro-warp">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-6 col-lg-7 p-0">
						<div class="intro-text">
							<h2>My name is Sam. I’m a photographer</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis commodo viverra maecenas. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
							<a href="#" class="sp-link">Take a look @my portfolio</a>
						</div>
					</div>
					<div class="col-xl-6 col-lg-5 p-0">
						<div class="skill-warp">
							<div class="single-progress-item">
								<div class="progress-bar-style" data-progress="75"></div>
								<p>Nature</p>
							</div>
							<div class="single-progress-item">
								<div class="progress-bar-style" data-progress="100"></div>
								<p>Passion</p>
							</div>
							<div class="single-progress-item">
								<div class="progress-bar-style" data-progress="90"></div>
								<p>Portraits</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end  -->
	
	<!-- Footer section   -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 order-1 order-md-2">
					<div class="footer-social-links">
						<a href=""><i class="fa fa-pinterest"></i></a>
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-dribbble"></i></a>
						<a href=""><i class="fa fa-behance"></i></a>
					</div>
				</div>
				<div class="col-md-6 order-2 order-md-1">
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>	
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end  -->

	<!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->

	<?php 

require_once "partials/fin-doc.part.php";

?>