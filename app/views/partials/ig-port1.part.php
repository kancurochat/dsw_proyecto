<?php 

// Partial empleado en el portfolio 2

shuffle($galeria);

foreach ($galeria as $foto) {

?>

<div class="mix col-lg-4 col-sm-6 p-0 <?= $foto->getNombreCategoria() ?>">
    <div class="portfolio-box">
        <div class="portfolio-item set-bg" data-setbg="<?= $foto->getURLPort1() ?>"></div>
        <h6><?php if($foto->getDescripcion() == '') echo 'Title'; else echo $foto->getDescripcion(); ?></h6>
        <p>2019</p>
    </div>
</div>
                
<?php } ?>