<?php 

	// Header del documento

?>

<!-- Header section  -->
<header class="header-section">
		<a href="/dsw_proyecto" class="site-logo"><img src="img/logo.png" alt="logo"></a>
		<div class="header-controls">
			<button class="nav-switch-btn"><i class="fa fa-bars"></i></button>
			
		</div>
		<ul class="main-menu">
			<li><a href="<?php if(isPage("index")) echo "#"; else echo "/dsw_proyecto" ?>">Home</a></li>
			<li><a href="<?php if(isPage("about")) echo "#"; else echo "about" ?>">About the Artist </a></li>
			<li>
				<a href="#">Portfolio</a>
				<ul class="sub-menu">
					<li><a href="<?php if(isPage("portfolio1")) echo "#"; else echo "portfolio1" ?>">Portfolio 1</a></li>
					<li><a href="<?php if(isPage("portfolio2")) echo "#"; else echo "portfolio2" ?>">Portfolio 2</a></li>
				</ul>
			</li>
			<li><a href="<?php if(isPage("blog")) echo "#"; else echo "blog" ?>">Blog</a></li>
			<li><a href="<?php if(isPage("elements")) echo "#"; else echo "elements" ?>">Post a photo</a></li>
			<li><a href="<?php if(isPage("contact")) echo "#"; else echo "contact" ?>">Contact</a></li>
			
		</ul>
	</header>

	<?php
	
	// COMPROBAR POR QUÉ NO FUNCIONA
	
	?>