<?php

// Partial empleado en la tabla que muestra los 
// mensajes subidos a la base de datos

foreach ($mensajes as $mensaje) {
?>

<tr>
    <td><?= $mensaje->getNombre() ?></td>
    <td><?= $mensaje->getEmail() ?></td>
    <td><?= $mensaje->getAsunto() ?></td>
    <td><?= $mensaje->getMensaje() ?></td>
</tr>

<?php
}
?>