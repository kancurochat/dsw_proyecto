<?php 

// Partial empleado para el portfolio 1

shuffle($galeria);

foreach ($galeria as $foto) {

?>

<div class="mix col-xl-2 col-md-3 col-sm-4 col-6 p-0 <?= $foto->getNombreCategoria() ?>">
    <a href="<?= $foto->getURLPort1() ?>" class="portfolio-item img-popup set-bg" data-setbg="<?= $foto->getURLPort1() ?>"></a>
</div>

<?php } ?>