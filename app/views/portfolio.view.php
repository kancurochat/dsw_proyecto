<?php 

require_once "partials/inicio-doc.part.php";

require_once "utils/utils.php";

?>

	<!-- Header section  -->
	<?php require_once "partials/nav.part.php"; ?>
	
	<div class="clearfix"></div>
	<!-- Header section end  -->

	<!-- Portfolio section  -->
	<div class="portfolio-section">
		<ul class="portfolio-filter controls text-center">
			<li class="control" data-filter="all">All</li>
			<li class="control" data-filter=".nature">Nature</li>
			<li class="control" data-filter=".studio">Studio Photography</li>
			<li class="control" data-filter=".weddings">Weddings</li>
			<li class="control" data-filter=".lifestyle">Lifestyle</li>
			<li class="control" data-filter=".fashion">Fashion</li>
		</ul>                                                           
		<div class="row portfolio-gallery m-0">
			<?php
			
			// Importamos el partial de las imágenes de esta galería
			require_once "partials/image-gallery.part.php"; 
			
			?>
			<div class="mix col-xl-2 col-md-3 col-sm-4 col-6 p-0">
				<div class="portfolio-item  next-btn">
					<h2>Next</h2>
				</div>
			</div>
		</div>
	</div>
	<!-- Portfolio section end  -->
	
	<!-- Footer section   -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 order-1 order-md-2">
					<div class="footer-social-links">
						<a href=""><i class="fa fa-pinterest"></i></a>
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-dribbble"></i></a>
						<a href=""><i class="fa fa-behance"></i></a>
					</div>
				</div>
				<div class="col-md-6 order-2 order-md-1">
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>	
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end  -->

	<!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->
	
	<?php 

require_once "partials/fin-doc.part.php";

?>