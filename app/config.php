<?php

// Array con la configuración para la conexión con la BD

return [

    "database" => [

        "name" => "proyecto",

        "username" => "user_proyecto",

        "password" => "server",

        "connection" => "mysql:host=localhost",

        "options" => [

            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,

            PDO::ATTR_PERSISTENT => true

        ]

    ]

];

?>