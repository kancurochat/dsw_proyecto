<?php

require_once "core/helpers/FlashMessage.php";
require_once "repository/CategoriaRepository.php";
require_once "core/App.php";
require_once "utils/File.php";

$mensaje = '';

try {
    
    $categoriaRepository = new CategoriaRepository();

    $categoriaSeleccionada = '';

    $categorias = $categoriaRepository->findAll();
    
    if($_SERVER["REQUEST_METHOD"] === "POST"){

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        FlashMessage::set("descripcion", $descripcion);

        $categoriaSeleccionada = trim(htmlspecialchars($_POST["categoria"]));

        FlashMessage::get("categoria", $categoriaSeleccionada);

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $rutaOrigen = "/var/www/html/" . "dsw_proyecto" . "/img/portfolio/";

        $imagen->saveUploadFile($rutaOrigen);
        $imagen->copyFile($rutaOrigen, "/var/www/html/dsw_proyecto/img/portfolio-2/");

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFileName(), $descripcion, $categoriaSeleccionada);

        $imagenGaleriaRepository->save($imagenGaleria);

        $_SESSION["mensajes"] = "Datos enviados";

        App::get('logger')->add("Imagen subida: " . $imagen->getFileName());

    }

    FlashMessage::unset("descripcion");
    FlashMessage::unset("categoria");

    $descripcion = '';
    $categoriaSeleccionada = '';

} catch (FileException $fileException) {

    FlashMessage::set("errores", [$fileException->getMessage()]);

} catch (AppException $appException) {

    FlashMessage::set("errores", [$appException->getMessage()]);

}catch (QueryException $queryException) {

    FlashMessage::set("errores", [$queryException->getMessage()]);

}

$errores = FlashMessage::get('errores');

unset($_SESSION["errores"]);

$mensaje = $_SESSION["mensajes"] ?? '';

unset($_SESSION["mensajes"]);

require_once "app/views/elements.view.php";

?>