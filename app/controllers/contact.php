<?php

require_once "entity/Mensaje.php";
require_once "repository/MensajeRepository.php";

$errors = [];
$fail = false;
$data = [];

$name = '';
$email = '';
$subject = '';
$message = '';

$mensajeRepository = new MensajeRepository();

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $name = htmlspecialchars(trim($_POST['name']));
    $email = htmlspecialchars(trim($_POST['email']));
    $subject = htmlspecialchars(trim($_POST['subject']));
    $message = htmlspecialchars(trim($_POST['message']));

    if($name == '' || $email == '' || $message == '') {

        $errors[] = "Name, email and message are required";
        $fail = true;

    }else {

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $errors[] = 'Insert a valid email';
            $fail = true;

        }

    }

}

$div = '';

if($fail) {

    $content = implode('\n', $errors);
    $div = "<div class=\"col-xs-12 col-sm-8 col-sm-push-2 h4 pt-5 mt-5 text-danger\">$content</div>";

}else {

    

    if($_SERVER['REQUEST_METHOD'] === 'POST') {

        $mensaje = new Mensaje(0, $name, $email, $subject, $message);
        $mensajeRepository->save($mensaje);

        $content = "Se ha mandado tu mensaje";
        $div = "<div class=\"col-xs-12 col-sm-8 col-sm-push-2 pt-5 mt-5 h4\">$content</div>";

    }

}

$mensajes = $mensajeRepository->findAll();

require_once "app/views/contact.view.php";

?>