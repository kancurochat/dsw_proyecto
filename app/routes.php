<?php 

// Array con las rutas amigables de nuestra aplicación

return [

    "dsw_proyecto" => "app/controllers/index.php",

    "dsw_proyecto/about" => "app/controllers/about.php",

    "dsw_proyecto/blog" => "app/controllers/blog.php",

    "dsw_proyecto/elements" => "app/controllers/elements.php",

    "dsw_proyecto/contact" => "app/controllers/contact.php",

    "dsw_proyecto/portfolio1" => "app/controllers/portfolio.php",

    "dsw_proyecto/portfolio2" => "app/controllers/portfolio-1.php"

];

?>